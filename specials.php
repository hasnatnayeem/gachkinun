<!DOCTYPE html>
<html >
<head>
<title>Gachkinun.com</title>
<link rel="stylesheet" type="text/css" href="style.css" />
</head>
<body>
<div id="wrap">
  <div class="header">
    <div class="logo"><img src="images/logo.png" alt="" border="0" width="150px" height="70px" /></div>
    <div id="menu">
     <ul>
        <li><a href="index.html">home</a></li>
        <li><a href="category.html">flowers</a></li>
		<li class="selected"><a href="specials.html">specials gifts</a></li>
        <li><a href="register.html">register</a></li>
        <li><a href="contact.html">contact</a></li>
		<li><a href="about.html">about us</a></li>
      </ul>
    </div>
  </div>
  <div class="center_content">
    <div class="left_content">
      <div class="title"><span class="title_icon"><img src="images/bullet1.gif" alt="" /></span>Special gifts</div>
      
   <?php   
      
       $con=mysqli_connect("localhost","root","","stall");

    if (mysqli_connect_errno()) {
	  echo "Failed to connect to MySQL: " . mysqli_connect_error();
	}
      
    $sql  = "SELECT * from product";
    
    $result = mysqli_query($con, $sql);

    while($row = mysqli_fetch_array($result))
    {
      echo  '<div class="feat_prod_box">
            <div class="prod_img"><a href=""><img src="product_images/' . $row['image'] .'" alt="" border="0" /></a></div>
            <div class="prod_det_box"> <span class="special_icon"><img src="images/special_icon.gif" alt="" /></span>
              <div class="box_top"></div>
              <div class="box_center">
                <div class="prod_title">'. $row['title'] .'</div>
                <p class="details">'. $row['description'] .'</p>
                <a href="" class="more">- more details -</a>
                <div class="clear"></div>
              </div>
              <div class="box_bottom"></div>
            </div>
            <div class="clear"></div>
          </div>';
    }    
      mysqli_close($con); 
      
      ?>
      
      
      
      <div class="pagination"> <span class="disabled"></span><span class="current">1</span><a href="">2</a><a href="">3</a>�<a href="">10</a><a href="">11</a><a href=""></a> </div>
      <div class="clear"></div>
    </div>
   <!--end of left content-->
    <div class="right_content">
      <div class="title"><span class="title_icon"><img src="images/bullet3.gif" alt="" /></span>About Our Shop</div>
      <div class="about">
        <p> <img src="images/about.gif" alt="" class="right" />  Gachkinun.com<br> address: flat no:7wb/3, <br> building: Bornali<br> lake city concord, Dhaka </p>
      </div>
      <div class="right_box">
        <div class="title"><span class="title_icon"><img src="images/bullet4.gif" alt="" /></span>Promotions</div>
        <div class="new_prod_box"> <a href="">product name</a>
          <div class="new_prod_bg"> <span class="new_icon"><img src="images/promo_icon.gif" alt="" /></span> <a href=""><img src="images/thumb1.gif" alt="" class="thumb" border="0" /></a> </div>
        </div>
        <div class="new_prod_box"> <a href="">product name</a>
          <div class="new_prod_bg"> <span class="new_icon"><img src="images/promo_icon.gif" alt="" /></span> <a href=""><img src="images/thumb2.gif" alt="" class="thumb" border="0" /></a> </div>
        </div>
        <div class="new_prod_box"> <a href="">product name</a>
          <div class="new_prod_bg"> <span class="new_icon"><img src="images/promo_icon.gif" alt="" /></span> <a href=""><img src="images/thumb3.gif" alt="" class="thumb" border="0" /></a> </div>
        </div>
      </div>
      <div class="right_box">
        <div class="title"><span class="title_icon"><img src="images/bullet5.gif" alt="" /></span>Categories</div>
        <ul class="list">
          <li><a href="">accesories</a></li>
          <li><a href="">flower gifts</a></li>
          <li><a href="">specials</a></li>
          <li><a href="">hollidays gifts</a></li>
          <li><a href="">accesories</a></li>
          <li><a href="">flower gifts</a></li>
          <li><a href="">specials</a></li>
          <li><a href="">hollidays gifts</a></li>
          <li><a href="">accesories</a></li>
          <li><a href="">flower gifts</a></li>
          <li><a href="">specials</a></li>
        </ul>
        
      </div>
    </div>
    <!--end of right content-->
    <div class="clear"></div>
  </div>
  <!--end of center content-->
  <div class="footer">
    <div class="left_footer"><img src="" height="70px" alt="" /><br />
  </div>
</div>
</html>
